---
title: "Introduction"
date: 2022-11-27T11:30:03+00:00
# weight: 1
# aliases: ["/first"]
tags: ["first"]
author: "dokuhebi"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: true
description: "A walk of a thousand miles..."
canonicalURL: "https://polity.space/posts/test/"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: true
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://github.com/<path_to_repo>/content"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link
---
Hi, and welcome to my new project. I've been wanting to compile information on Church polity for a while, but
recent events at my church and events from friends pushed me over the edge. My goal for this site is to create
a resource for people in the church to learn from various primary resources on governing the Church.

If you thought there were differences amongst Christians related to theology, wait until you dig into polity. 
Even within the Reformed Christian churches that I have been part of for the last 40+ years, otherwise honorable
people have had great disagreements on the Biblical way to organize and govern the Church.

This will be a long project and I'm currently just one guy working on it, so don't expect it to end up being
a go-to resource anytime soon. My vision is to have different views presented by their own advocates, along
with historical resources from Church history. Compiling and posting will take time, but I believe it's a
project worth doing.

